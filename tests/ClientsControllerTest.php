<?php

use App\Http\Controllers\ClientsController;

class ClientsControllerTest extends TestCase
{
    public function testClientsCsvIsReadable()
    {
        $clientsCsvFile = storage_path().'/clients.csv';
        $this->assertTrue(is_readable($clientsCsvFile), $clientsCsvFile . ' is not readable.');
    }

    public function testClientsCsvIsWritable()
    {
        $clientsCsvFile = storage_path().'/clients.csv';
        $this->assertTrue(is_writeable($clientsCsvFile), $clientsCsvFile . ' is not writable.');
    }   

    public function testClientsCsvIsNotEmpty()
    {
        $clientsController = new ClientsController;
        $clientsList = $clientsController->getAllClients();
        $this->assertGreaterThanOrEqual(1, count($clientsList));
    }

}
